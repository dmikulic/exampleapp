from starlette.routing import Router, Mount

from djproj.asgi import application as dj_app_asgi
from asyncapp.main import app as fastapi_asgi

app = Router(
    [
        Mount("/dj", app=dj_app_asgi, name="Django App"),
        Mount("", app=fastapi_asgi, name="FastAPI")   
    ]
)