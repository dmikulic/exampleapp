from fastapi import FastAPI
from django.contrib.contenttypes.models import ContentType

app = FastAPI()


@app.get("/")
async def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
async def read_item(item_id: int, q: str = None):
    return {"item_id": item_id, "q": q}


@app.get("/read")
def dj_read():
    ct = [c.name for c in ContentType.objects.all()]
    return {'django__contenttypes': ct}