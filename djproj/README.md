`pip install -r requirements.txt`

`python manage.py migrate`

`gunicorn multi:app -w 2 -k uvicorn.workers.UvicornWorker`
OR
`uvicorn multi:app`


http://localhost:8000/dj/

http://localhost:8000/read

http://localhost:8000/items/123

DOCS
http://localhost:8000/docs

http://localhost:8000/redoc

